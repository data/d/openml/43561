# OpenML dataset: Accidents-on-the-Rio-Niteri-bridge

https://www.openml.org/d/43561

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
About the Rio  Niteri Bridge, according to Wikipedia:
Presidente Costa e Silva Bridge, popularly known as Rio  Niteri Bridge, is a bridge that crosses Guanabara Bay, in the state of Rio de Janeiro, in Brazil. It connects the municipalities of Rio de Janeiro and Niteri.
Currently, the bridge is the largest in the southern hemisphere in prestressed concrete and the largest in Latin America. The structure receives more than 150 thousand passengers a day, according to information from the Ecoponte concessionaire, on days with normal flows. [1] It is also known as the largest straight span in the world and the largest set of prestressed structures in America. [2]
Content
The data to carry out this research were obtained on the website of the Federal Highway Police, which provides an Open Data session which, according to the website: has no restrictions on licenses, patents or control mechanisms, so that they are freely available for use and redistributed at will . In the Accidents section it is possible to find accident records in csv format over the years.
Acknowledgements
It is great that this data is public and can present valuable insights to improve people's quality of life
Inspiration
Some guiding questions about this data:

Is the number of accidents on the bridge daily?
Have the number of accidents on the bridge decreased?
Has the installation of security cameras reduced the number of accidents?

How to import from source
See below the code used to import and organize the open data of the site: 
 Load tidyverse:
library(tidyverse)

 Import available data:
dataset - 
  glue::glue("datatran2007:2020.csv")  
  map(.x 
        data.table::fread(sep = ";",dec=",", encoding = "Latin-1") 
        as_tibble() )

 Perform data pre-processing in parallel using the foreach package:
library(foreach)
cl - parallel::makeCluster(4)
doParallel::registerDoParallel(cl)
dataset - foreach(x = dataset, 
        info = 2007:2020, 
        .packages = c("dplyr", "stringr", "lubridate")) dopar 

          x  
            mutate(km = as.character(km),
                   br = as.character(br))  
            mutate_if(is.character,
                      .x 
                        enc2native() 
                        stringi::stri_trans_general(id = "Latin-ASCII")  
                        tolower()  
                        str_replace_all("/","-")  
                        str_replace_all(",",".") ) 
            mutate(data_inversa = if_else(is.na(dmy(data_inversa)),
                                          ymd(data_inversa),
                                          dmy(data_inversa)) ,
                   km = as.numeric(km),
                   br = as.numeric(br),
                   id = as.character(id),
                   dia_semana = str_remove_all(dia_semana,"-feira")
            ) 
            filter(br==101  km=321  km334)   
            slice(str_which(municipio,"(niteroirio de janeiro)"))
        
parallel::stopCluster(cl)

 Save the tidy dataset:
dataset  
  map2_df(2007:2020, mutate(.x,ano = .y))  
  write_csv('accidents-rio-niteroi-bridge.csv', na = "")

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43561) of an [OpenML dataset](https://www.openml.org/d/43561). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43561/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43561/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43561/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

